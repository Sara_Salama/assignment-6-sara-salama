package assignment6;

public class Candy extends DessertItem {
	
	private double Weight;
	private int PricePerPound;
	//private String flavor;

	public Candy(String flavor,double Weight,int PricePerPound) {
		super(flavor);
		//this.flavor=flavor;
		this.Weight=Weight;
		this.PricePerPound=PricePerPound;
	}

	@Override
	public int getCost() {
		// TODO Auto-generated method stub
		
		int  cost =(int) Math.round(this.Weight*this.PricePerPound);
		return cost;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		 System.out.println("candy:"+"     "+this.getName()+"  "+this.Weight+" lbs"+"  "+this.PricePerPound+"/lb."+"                            "+DessertShoppe.cents2dollarsAndCents(this.getCost())+ "$");

	}

	
	

}
