package assignment6;

public class IceCream extends DessertItem {

	protected int cost;
	//private String flavor;
	
	public IceCream(String flavor, int cost) {
		// TODO Auto-generated constructor stub
		super(flavor);
		this.cost=cost;
		
		
	}

	@Override
	public int getCost() {
		
		 return this.cost;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		 System.out.println("IceCream:  "+this.getName()+"   "+this.cost+" cents                         "+DessertShoppe.cents2dollarsAndCents(this.getCost())+ "$");

	}
	
	
}
