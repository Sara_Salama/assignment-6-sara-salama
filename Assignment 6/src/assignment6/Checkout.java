package assignment6;

import java.util.ArrayList;

public class Checkout {
	
	private ArrayList <DessertItem> cashRegister=new ArrayList <DessertItem>() ;
	
	public ArrayList getCashRegister() {

		return cashRegister;
	}
	public void enterItem(DessertItem item) {
		 cashRegister.add(item);
	}
	public int numberOfItems() {
		return cashRegister.size();
	}
	public int totalCost() {
		int cost=0;
		for (int i=0; i<cashRegister.size();i++) {
			cost+=(cashRegister.get(i).getCost());
		}
		return cost;
	}
	public int totalTax() {
		int tax=0;
		for (int i=0; i<cashRegister.size();i++) {
			tax+=(cashRegister.get(i).getCost()*0.02);
		}
		return tax;
	}
	public void clear() {
		cashRegister.clear();
	}
	public  void print() {
		for (int i=0; i<cashRegister.size();i++) {
			cashRegister.get(i).print();
		}
	}
	
	
	
}
