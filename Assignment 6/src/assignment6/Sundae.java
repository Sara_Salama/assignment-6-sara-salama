package assignment6;

public class Sundae extends IceCream {
	
	private int toppingCost;
	private String toppingFlavor;
	
	
	public Sundae(String IceCreamflavor, int IceCreamcost , String flavor, int cost ) {
		super(IceCreamflavor, IceCreamcost);
		// TODO Auto-generated constructor stub
		this.toppingCost=cost;
		this.toppingFlavor= flavor;
	}

	public  int getCost() {
		int cost =this.toppingCost;
		return cost+super.getCost();
	}
	
	public void print() {
		 System.out.println("ice cream: "+super.getName()+"  "+super.cost+" cents  "+"topping: "+this.toppingFlavor+" "+this.toppingCost+" cents "+" "+DessertShoppe.cents2dollarsAndCents(this.getCost())+ "$");

		
	}

}
