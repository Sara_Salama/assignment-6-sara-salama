package assignment6;

public class Cookie extends DessertItem{

	//private String flavor;
	private  int priceperdozen;
	private int number;
	
	
	public Cookie(String flavor, int number, int priceperdozen) {
		super(flavor);
		//this.flavor=flavor;
		this.number=number;
		this.priceperdozen=priceperdozen;
	}

	@Override
	public int getCost() {
		// TODO Auto-generated method stub
		int cost= Math.round(this.number*(this.priceperdozen/12));
		return cost;
	}

	@Override
	public void print() {
		 System.out.println("cookies:   "+this.getName()+"  "+this.number+" cookies "+this.priceperdozen+" cents /dz. "+"        "+DessertShoppe.cents2dollarsAndCents(this.getCost())+"$");

		
	}

}
